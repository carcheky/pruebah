#!/bin/bash

phpcs --standard=Drupal --extensions=md,install,module,inc,yml,js,css,scss,theme,twig .
phpcbf --standard=Drupal --extensions=md,install,module,inc,yml,js,css,scss,theme,twig .
