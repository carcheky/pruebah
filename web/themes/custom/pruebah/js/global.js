/**  * @file  */
(function ($) {
  Drupal.behaviors.tpv_aperitivo = {
    attach: function (context, settings) {

      // mostramos los primeros 3 elementos
      $('#block-pruebah-footer .menu li:lt(' + 3 + ')').show();
      // añadimos el botón para desplegar
      $('#block-pruebah-footer .menu').append('<li id="loadmore" style="display:block !important;"><a>+</a></li>');

      $('#loadmore a').click(function () {
        // mostramos todos los elementos del menú
        $('#block-pruebah-footer .menu li').show();
        // escondemos el botón para desplegar
        $('#loadmore').hide();
      });

    }
  };
})(jQuery);
